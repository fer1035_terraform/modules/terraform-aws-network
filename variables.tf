variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR range."
  default     = "10.0.0.0/16"
}

variable "subnet_public_1_cidr" {
  type        = string
  description = "Public Subnet 1 CIDR range."
  default     = "10.0.1.0/24"
}

variable "subnet_public_2_cidr" {
  type        = string
  description = "Public Subnet 2 CIDR range."
  default     = "10.0.4.0/24"
}

variable "subnet_private_1_cidr" {
  type        = string
  description = "Private Subnet 1 CIDR range."
  default     = "10.0.2.0/24"
}

variable "subnet_private_2_cidr" {
  type        = string
  description = "Private Subnet 2 CIDR range."
  default     = "10.0.3.0/24"
}

variable "subnet_public_1_az" {
  type        = string
  description = "Public Subnet 1 Availability Zone."
  default     = "us-east-1a"
}

variable "subnet_public_2_az" {
  type        = string
  description = "Public Subnet 1 Availability Zone."
  default     = "us-east-1d"
}

variable "subnet_private_1_az" {
  type        = string
  description = "Private Subnet 1 Availability Zone."
  default     = "us-east-1b"
}

variable "subnet_private_2_az" {
  type        = string
  description = "Private Subnet 2 Availabillity Zone."
  default     = "us-east-1c"
}
